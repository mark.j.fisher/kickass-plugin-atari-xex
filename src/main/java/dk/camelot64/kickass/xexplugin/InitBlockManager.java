package dk.camelot64.kickass.xexplugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kickass.plugins.interf.general.IMemoryBlock;

import static dk.camelot64.kickass.xexplugin.AtariXex.addWord;
import static dk.camelot64.kickass.xexplugin.AtariXex.toByteArray;

/**
 * Keeps track of INIT blocks that have to be added to the XEX file.
 * The INIT blocks are added by the {@link XexInitSegmentModifier} and read by the {@link XexFormatSegmentModifier}
 */
public class InitBlockManager {
    private final List<String> blocksProcessed = new ArrayList<>();
    private final Map<String, InitBlock> singleSegmentBlocks = new HashMap<>();
    private final List<InitBlock> multiSegmentBlocks = new ArrayList<>();

    public void add(List<IMemoryBlock> memoryBlocks, int initAddr) {
        if (memoryBlocks.size() == 1) {
            // A single segment block has no dependencies.
            IMemoryBlock b = memoryBlocks.get(0);
            InitBlock v = new InitBlock(b.getName(), Collections.emptyList(), initAddr);
            singleSegmentBlocks.put(b.getName(), v);
        } else {
            // This is a multi-part segment, example:
            // .segmentdef Program [segments="Code1, Code2", modify="XexInit", _InitAddr=load3]

            // We will get 2 memory blocks named "Code1", "Code2".
            // This multipart segment needs to depend on them before it outputs its init block.

            // Sadly we don't get the name "Program", so we have to make a name up. Just join the segments names together.
            String name = memoryBlocks.stream().map(IMemoryBlock::getName).collect(Collectors.joining(","));
            // Now get the names of the blocks as our dependencies, e.g. "Code1", "Code2"
            List<String> dependencies = memoryBlocks.stream().map(IMemoryBlock::getName).collect(Collectors.toList());

            // Create the init block for this "multi-part" segment
            InitBlock v = new InitBlock(name, dependencies, initAddr);
            multiSegmentBlocks.add(v);
        }
    }

    public byte[] getBytes(String name) {
        // when something asks for the bytes for (e.g.) "Code1" we output any single segment blocks with this name.
        // We also look for multi-part segments that have had all their dependecies output, and then output it if they have.

        List<Byte> output = new ArrayList<>();
        InitBlock b = singleSegmentBlocks.get(name);
        if (b != null) {
            // Single segment case, just output the data. Single segments will never be processed multiple times.
            addWord(output, 0x02e2);
            addWord(output, 0x02e3);
            addWord(output, b.initAddress);

            // Add ourselves to the blocks processed
            blocksProcessed.add(name);
        }

        // Are there any unprocessed multi-segment blocks that have all their dependent segments complete?
        for(InitBlock multiSegmentBlock: multiSegmentBlocks) {
            if (!multiSegmentBlock.processed && blocksProcessed.containsAll(multiSegmentBlock.dependencies)) {
                // we can output this multi-segment block
                addWord(output, 0x02e2);
                addWord(output, 0x02e3);
                addWord(output, multiSegmentBlock.initAddress);

                // mark it as done
                multiSegmentBlock.processed = true;
            }
        }

        return toByteArray(output);
    }

    public byte[] remainingBlocks() {
        // These are init blocks that have segments that have no init blocks themselves, so just need to be output.
        // e.g. .segmentdef Program [segments="Code1, Code2", modify="XexInit", _InitAddr=load3]

        List<Byte> output = new ArrayList<>();
        for(InitBlock multiSegmentBlock: multiSegmentBlocks) {
            if (!multiSegmentBlock.processed) {
                addWord(output, 0x02e2);
                addWord(output, 0x02e3);
                addWord(output, multiSegmentBlock.initAddress);
            }
        }

        return toByteArray(output);
    }

    /**
     * Clear the managed blocks.
     */
    public void clear() {
        singleSegmentBlocks.clear();
        multiSegmentBlocks.clear();
        blocksProcessed.clear();
    }

    // A helper class for storing related information about the block. Only used internally by the manager itself.
    private static class InitBlock {
        String name;
        List<String> dependencies;
        int initAddress;
        // only used in multi-segment blocks
        boolean processed;

        InitBlock(String name, List<String> dependencies, int initAddress) {
            this.name = name;
            this.dependencies = dependencies;
            this.initAddress = initAddress;
            processed = false;
        }
    }

}
