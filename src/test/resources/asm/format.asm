// Tests the XEX plugin - simple XEX format
.plugin "dk.camelot64.kickass.xexplugin.AtariXex"
.file [name="format.xex", type="bin", segments="File"]
.segmentdef File [segments="Program", modify="XexFormat"]
.segmentdef Program [segments="Code, Data" ]
.segmentdef Code [start=$2000]
.segmentdef Data [start=$3000]

.segment Code
// Show a simple raster bar
raster:
    ldx #0
!:  lda data,x
    sta $d01a // GTIA COLBK
    eor #$50
    sta $d018 // GTIA COLPF2
    sta $d40a // ANTIC WSYNC
    inx
    cpx #$10
    bne !-
start:
    lda #0
!:  cmp $d40b
    bne !-
    jmp raster

.segment Data
data: .fill $10, i