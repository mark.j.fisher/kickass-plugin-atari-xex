package dk.camelot64.kickass.xexplugin;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import kickass.plugins.impl.PluginMemoryBlock;
import kickass.plugins.interf.general.IEngine;
import kickass.plugins.interf.general.IMemoryBlock;
import kickass.plugins.interf.general.IParameterMap;
import kickass.plugins.interf.general.IValue;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static dk.camelot64.kickass.xexplugin.TestUtil.asByteArray;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class XeXSegmentModifierTest {

    IEngine engine = mock(IEngine.class);
    IParameterMap parameterMap = mock(IParameterMap.class);
    IValue runAddress = mock(IValue.class);
    IMemoryBlock memoryBlock = mock(IMemoryBlock.class);
    InitBlockManager initBlockManager = mock(InitBlockManager.class);

    @Test
    void testRunAddrSegmentParameterAndNoInitBlocksOnlyAddsAFinalRunAddress() {
        // Given
        XexFormatSegmentModifier modifier = new XexFormatSegmentModifier(initBlockManager);

        PluginMemoryBlock block1 = new PluginMemoryBlock("block1", 0x1234, asByteArray(Arrays.asList(0xa9, 0x00)));
        List<IMemoryBlock> blocks = Collections.singletonList(block1);

        // Interactions
        when(parameterMap.exist("_RunAddr")).thenReturn(true);
        when(parameterMap.getValue("_RunAddr")).thenReturn(runAddress);
        when(runAddress.hasIntRepresentation()).thenReturn(true);
        when(runAddress.getInt()).thenReturn(0x2000);
        when(engine.createMemoryBlock(Mockito.anyString(), Mockito.anyInt(), Mockito.any())).thenReturn(memoryBlock);
        when(initBlockManager.getBytes("block1")).thenReturn(new byte[0]);
        when(initBlockManager.remainingBlocks()).thenReturn(new byte[0]);

        // When
        List<IMemoryBlock> results = modifier.execute(blocks, parameterMap, engine);

        // Then
        assertEquals(results.get(0), memoryBlock);

        byte[] expectedArray = asByteArray(Arrays.asList(
            0xff, 0xff,                 // binary header
            0x34, 0x12, 0x35, 0x12,     // start address, end address
            0xa9, 0x00,                 // block's data
            0xe0, 0x02, 0xe1, 0x02,     // RUN ADDR start,end ($2e0,$2e1)
            0x00, 0x20                  // address to run at
        ));
        verify(engine).createMemoryBlock("xex_format", 0, expectedArray);
    }

    @Test
    void testInitAddrSegmentParameterCausesSegmentModifierToGetInitBlockDataForNamedBlock() {
        XexFormatSegmentModifier modifier = new XexFormatSegmentModifier(initBlockManager);
        PluginMemoryBlock block = new PluginMemoryBlock("block1", 0x1234, asByteArray(Arrays.asList(0xa9, 0x00)));
        List<IMemoryBlock> blocks = Collections.singletonList(block);

        // Interactions, no run address
        when(parameterMap.exist("_RunAddr")).thenReturn(false);
        when(engine.createMemoryBlock(Mockito.anyString(), Mockito.anyInt(), Mockito.any())).thenReturn(memoryBlock);
        when(initBlockManager.getBytes("block1")).thenReturn(asByteArray(Arrays.asList(0xe2, 0x02, 0xe3, 0x02, 0x45, 0x23)));
        when(initBlockManager.remainingBlocks()).thenReturn(new byte[0]);

        // When
        List<IMemoryBlock> results = modifier.execute(blocks, parameterMap, engine);

        // Then
        assertEquals(results.get(0), memoryBlock);

        byte[] expectedArray = asByteArray(Arrays.asList(
            0xff, 0xff,                         // binary header
            0x34, 0x12, 0x35, 0x12,             // start address, end address
            0xa9, 0x00,                         // block's data
            0xe2, 0x02, 0xe3, 0x02, 0x45, 0x23 // INIT block
        ));
        verify(engine).createMemoryBlock("xex_format", 0, expectedArray);
    }

}
