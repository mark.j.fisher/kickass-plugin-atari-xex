package dk.camelot64.kickass.xexplugin;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;

public class TestUtil {
    static String getResourcePath(String resourcePath) throws URISyntaxException {
        final URL resource = TestUtil.class.getResource(resourcePath);
        if(resource==null)
            throw new RuntimeException("Test resource not found "+resourcePath);
        return Paths.get(resource.toURI()).toAbsolutePath().toString();
    }

    static byte[] asByteArray(List<Integer> inputs) {
        byte[] byteArray = new byte[inputs.size()];
        for (int i = 0; i < inputs.size(); i++) {
            byteArray[i] = inputs.get(i).byteValue();
        }
        return byteArray;
    }
}
