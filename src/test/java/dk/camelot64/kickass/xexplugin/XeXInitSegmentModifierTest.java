package dk.camelot64.kickass.xexplugin;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import kickass.plugins.impl.PluginMemoryBlock;
import kickass.plugins.interf.general.IEngine;
import kickass.plugins.interf.general.IMemoryBlock;
import kickass.plugins.interf.general.IParameterMap;
import kickass.plugins.interf.general.IValue;
import org.junit.jupiter.api.Test;

import static dk.camelot64.kickass.xexplugin.TestUtil.asByteArray;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class XeXInitSegmentModifierTest {

    IEngine engine = mock(IEngine.class);
    IParameterMap parameterMap = mock(IParameterMap.class);
    IValue initAddress = mock(IValue.class);
    InitBlockManager initBlockManager = mock(InitBlockManager.class);

    @Test
    void testManagerAddsABlockWhenInitSegmentManagerIsExecuted() {
        PluginMemoryBlock block1 = new PluginMemoryBlock("block1", 0x1234, asByteArray(Arrays.asList(0xa9, 0x00)));
        List<IMemoryBlock> blocks = Collections.singletonList(block1);

        when(parameterMap.exist("_InitAddr")).thenReturn(true);
        when(parameterMap.getValue("_InitAddr")).thenReturn(initAddress);
        when(initAddress.hasIntRepresentation()).thenReturn(true);
        when(initAddress.getInt()).thenReturn(0x2345);

        XexInitSegmentModifier initSegmentModifier = new XexInitSegmentModifier(initBlockManager);

        // When
        List<IMemoryBlock> returnedBlocks = initSegmentModifier.execute(blocks, parameterMap, engine);

        // Then the init address specified is used with the given blocks
        verify(initBlockManager).add(blocks, 0x2345);
        // But that's all
        verifyNoMoreInteractions(initBlockManager);

        assertEquals(blocks, returnedBlocks);
    }
}
