package dk.camelot64.kickass.xexplugin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.List;
import kickass.KickAssembler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static dk.camelot64.kickass.xexplugin.TestUtil.getResourcePath;

public class TestXexPlugin {

    @Test
    // Test minimal Kick Assembler compilation
    void testKickAssMin() throws URISyntaxException, IOException {
        kickAss("min", 0, false);
    }

    @Test
    // Test Bad assembly fails
    void testBadAssembly() throws URISyntaxException, IOException {
        kickAss("bad", 1, false);
    }

    @Test
    // Test XEX format
    void testXexFormat() throws URISyntaxException, IOException {
        kickAss("format", 0, true);
    }

    @Test
    // Test RUN block
    void testXexRun() throws URISyntaxException, IOException {
        kickAss("run", 0, true);
    }

    @Test
    // Test INIT blocks
    void testXexInit() throws URISyntaxException, IOException {
        kickAss("init", 0, true);
    }

    @Test
    // Test INIT blocks - outer segment
    void testXexInit2() throws URISyntaxException, IOException {
        kickAss("init2", 0, true);
    }

    @Test
        // Test INIT blocks - nested segments
    void testXexInit3() throws URISyntaxException, IOException {
        kickAss("init3", 0, true);
    }

    @Test
    // Test INIT blocks - consecutive memory blocks
    void testXexInit4() throws URISyntaxException, IOException {
        kickAss("init4", 0, true);
    }

    @Test
    // Test INIT blocks - Order of segments given different to memory order
    void testXexInit5() throws URISyntaxException, IOException {
        kickAss("init5", 0, true);
    }

    private void kickAss(String fileName, int expectedExit, boolean compareXex) throws URISyntaxException, IOException {
        int exit = KickAssembler.main2(new String[]{getResourcePath("/asm/" + fileName + ".asm"), "-odir", getResourcePath("/out/asm"), "-showmem"});
        Assertions.assertEquals(expectedExit, exit);

        if(compareXex) {
            final File xexFile = new File(getResourcePath("/out/asm/" + fileName + ".xex"));
            final File hexFile = new File(getResourcePath("/out/asm") + "/" + fileName + ".hex");
            writeHexFile(xexFile, hexFile);
            System.out.println("Wrote hex file to "+hexFile.getAbsolutePath());

            final File hexRefFile = new File(getResourcePath("/ref/" + fileName + ".hex"));
            List<String> refLines = Files.readAllLines(hexRefFile.toPath());
            List<String> hexLines = Files.readAllLines(hexFile.toPath());
            assertLines(refLines, hexLines);
        }
    }

    /**
     * Assert that expected lines match actual lines
     * @param expected The expected lines
     * @param actual The actual lines
     */
    void assertLines(List<String> expected, List<String> actual) {
        for (int i = 0; i < actual.size(); i++) {
            String outLine = actual.get(i);
            if(expected.size()>i) {
                String refLine = expected.get(i);
                if(!outLine.trim().equals(refLine.trim())) {
                    Assertions.fail(
                            "Output does not match reference on line "+i+"\n"+
                                    "Reference: >"+refLine+"<\n"+
                                    "Output:    >"+outLine+"<"
                    );
                }
            } else {
                Assertions.fail(
                        "BOutput does not match reference on line "+i+"\n"+
                                "Reference: <EOF>\n"+
                                "Output:    "+outLine
                );
            }
        }
    }

    /**
     * Writes a HEX file containing hex formatted bytes from a binary file
     * @param binFile The binary file
     * @param hexFile The hex file
     * @throws IOException
     */
    private void writeHexFile(File  binFile, File hexFile) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(hexFile));
        final byte[] xexBytes = Files.readAllBytes(binFile.toPath());
        int i=0;
        for (byte xexByte : xexBytes) {
            writer.write(String.format("%02x", xexByte));
            i=(i+1)%16;
            if(i==0)
                writer.write("\n");
            else
                writer.write(" ");
        }
        if(i!=0)  writer.write("\n");
        writer.close();
    }
    



}
