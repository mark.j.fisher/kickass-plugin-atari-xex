# Kick Assembler Atari XEX Format Plugin

Plugin for generating Atari 8bit Executable XEX-files using the MOS 65xx assembler Kick Assembler. 
See http://www.theweb.dk/KickAssembler

The XEX format is a binary executable format containing multiple memory blocks.
See https://www.atarimax.com/jindroush.atari.org/afmtexe.html

## Download

You can download the plug-in JAR here https://gitlab.com/jespergravgaard/kickass-plugin-atari-xex/-/releases

Alternatively you can build it yourself. See the section Building below.

# Usage

In your Kick Assembler program you use the plugin by
- Adding `KickAssXexFormat-<version>.jar` to the java classpath when running KickAss
- Use the `.plugin` directive in your source file to tell KickAss to use the plugin
- Use the segment parameter `modify="XexFormat"` in a segment definition.
- Optionally set the program RUN address by using the segment parameter `_RunAddr` (either to an integer or a label)
- Optionally add INIT addresses to code blocks by using the segment parameter `modify="XexInit"` in a segment definition and the segment parameter `_InitAddr` (either to an integer or a label).
- Optionally add an ordering to the segment loading using the segment parameter `_Order` as a list of the segment names. Any remaining segments will be add to the end of the list. By default, segments order is ascending memory address. This parameter must be on the `XexFormat` modifier.

Here is KickAss code to output the Segment `Program` in XEX-format to the file `program.xex`, where the program starts at the label `start`:
```
  .plugin "dk.camelot64.kickass.xexplugin.AtariXex"
  .file [name="program.xex", type="bin", segments="Program", modify="XexFormat", _RunAddr=start]
```

And KickAss code to output the Segment `Program` in XEX-format to the file `program.xex`, where `init` is called during loading and the program starts at `start`:
```
  .plugin "dk.camelot64.kickass.xexplugin.AtariXex"
  .file [name="program.xex", type="bin", segments="Program", modify="XexFormat", _RunAddr=start]
  .segmentdef Program [segments="Code, Data" ]
  .segmentdef Code [start=$2000, modify="XexInit", _InitAddr=init]
  .segmentdef Data [startAfter="Code"]
```

You can see more example programs here: https://gitlab.com/jespergravgaard/kickass-plugin-atari-xex/-/tree/master/src/test/resources/asm

## Building

The prerequisites for locally building the JAR is Java 8+.

The project uses gradle to build.

The steps to building the JAR is

1. Download, Fork or Clone the source code from https://gitlab.com/jespergravgaard/kickass-plugin-atari-xex
2. Build the jar with

        # Any unix variant
        ./gradlew
    
        # Windows
        gradlew.bat

3. The new JAR is found in the /bin/ folder
