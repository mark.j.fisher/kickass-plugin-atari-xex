plugins {
    // kotlin includes java, so no harm making it part of the build.
    kotlin("jvm") version "1.4.10"
}

// Set the plugin version here, it will be included in the jar file's name
version = "1.3"

// Change name of the generated JAR
val kickAssXexArchiveBaseName = "KickAssXexFormat"

// Library versions
val kickAssemblerVersion = "5.16"
val junitJupiterEngineVersion = "5.7.0"
val mockitoVersion = "3.6.0"

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(files("jars/kickassembler-$kickAssemblerVersion.jar")) // A shame this isn't a published resource
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitJupiterEngineVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitJupiterEngineVersion")
    testImplementation("org.mockito:mockito-core:$mockitoVersion")
}

tasks {
    register("package", Copy::class.java) {
        dependsOn("build")
        from(jar)
        into("bin")
    }

    jar {
        manifest {
            // Base values
            from("src/main/resources/META-INF/MANIFEST.MF")
            // Add version from the plugin version
            attributes(mapOf("Implementation-Version" to archiveVersion.get()))
        }
        // The default is "<project name from settings.gradle>", we override it to suit our output
        archiveBaseName.set(kickAssXexArchiveBaseName)
    }

    getByName<Wrapper>("wrapper") {
        gradleVersion = "6.7"
        distributionType = Wrapper.DistributionType.ALL
    }

    named<Test>("test") {
        useJUnitPlatform()
    }

    compileJava {
        sourceCompatibility = "1.8"
        targetCompatibility = "1.8"
    }

}


defaultTasks(
        "clean", "package"
)

repositories {
    mavenCentral()
    jcenter()
}
